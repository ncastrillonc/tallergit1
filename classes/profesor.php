<?php
    class profesor extends object_standard {
        
        //attribute variables
        protected $cedula;
        protected $nombre;
        protected $fechanac;
        protected $lugarnac;
        protected $titulo;
        protected $deto;

        //components
        var $components = array();

        //auxiliars for primary key and for files
        var $auxiliars = array();

        //data about the attributes
        public function metadata() {
            return array("cedula" => array(), "nombre" => array(), "fechanac" => array(), 
                "lugarnac" => array(), "titulo" => array(), "depto" => array());
        }

        public function primary_key() {
            return array("cedula");
        }

        public function relational_keys($class, $rel_name) {
            switch($class) {
                default:
                break;
            }
        }
    }
?>
